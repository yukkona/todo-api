FROM node:alpine
WORKDIR /app
COPY . .
RUN npm ci
EXPOSE 1337/tcp
ENV NODE_ENV=production
CMD ["node", "app.js"]